<?php
/**
 * @file
 * paddle_holiday_participation.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function paddle_holiday_participation_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_paddle_offer_fields|node|offer|form';
  $field_group->group_name = 'group_paddle_offer_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'offer';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Offer fields',
    'weight' => '1',
    'children' => array(
      0 => 'field_hp_category',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-paddle-offer-fields field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_paddle_offer_fields|node|offer|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Offer fields');

  return $field_groups;
}
