<?php
/**
 * @file
 * paddle_holiday_participation.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function paddle_holiday_participation_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'offer';
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->name = 'node:offer:default';
  $panelizer->css_id = '';
  $panelizer->css_class = '';
  $panelizer->css = '';
  $panelizer->no_blocks = FALSE;
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $panelizer->pipeline = 'paddle_content_region';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $display = new panels_display();
  $display->layout = 'paddle_2_col_9_3_bottom';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'right' => NULL,
      'left' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'ca3a0478-06f7-4c69-bbed-99b5b8f486b4';
  $display->storage_type = 'panelizer_default';
  $display->storage_id = 'node:offer:default';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-22b14eae-6d58-45c8-a6f0-e6b6b5a615e9';
  $pane->panel = 'bottom';
  $pane->type = 'content_region';
  $pane->subtype = 'inherit';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'region' => 'bottom',
    'type' => 'offer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array(
    'type' => 'immovable',
    'regions' => array(
      'bottom' => 'bottom',
    ),
  );
  $pane->uuid = '22b14eae-6d58-45c8-a6f0-e6b6b5a615e9';
  $display->content['new-22b14eae-6d58-45c8-a6f0-e6b6b5a615e9'] = $pane;
  $display->panels['bottom'][0] = 'new-22b14eae-6d58-45c8-a6f0-e6b6b5a615e9';
  $pane = new stdClass();
  $pane->pid = 'new-0cc8bf88-e8bf-4d5b-b955-0cae8ed25508';
  $pane->panel = 'left';
  $pane->type = 'entity_revision_view';
  $pane->subtype = 'node';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array(
    'type' => 'immovable',
    'regions' => array(
      'left' => 'left',
    ),
  );
  $pane->uuid = '0cc8bf88-e8bf-4d5b-b955-0cae8ed25508';
  $display->content['new-0cc8bf88-e8bf-4d5b-b955-0cae8ed25508'] = $pane;
  $display->panels['left'][0] = 'new-0cc8bf88-e8bf-4d5b-b955-0cae8ed25508';
  $pane = new stdClass();
  $pane->pid = 'new-b7280f04-4873-4b3f-9a4b-5da23aaa7df2';
  $pane->panel = 'right';
  $pane->type = 'content_region';
  $pane->subtype = 'inherit';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'region' => 'right',
    'type' => 'offer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array(
    'type' => 'immovable',
    'regions' => array(
      'right' => 'right',
    ),
  );
  $pane->uuid = 'b7280f04-4873-4b3f-9a4b-5da23aaa7df2';
  $display->content['new-b7280f04-4873-4b3f-9a4b-5da23aaa7df2'] = $pane;
  $display->panels['right'][0] = 'new-b7280f04-4873-4b3f-9a4b-5da23aaa7df2';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:offer:default'] = $panelizer;

  return $export;
}
