<?php
/**
 * @file
 * paddle_holiday_participation.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function paddle_holiday_participation_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_hp_category'.
  $field_bases['field_hp_category'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hp_category',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'holiday accommodations' => 'Holiday accommodations',
        'group accommodations' => 'Group accommodations',
        'day trips' => 'Day trips',
        'organised holidays' => 'Organised holidays',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
